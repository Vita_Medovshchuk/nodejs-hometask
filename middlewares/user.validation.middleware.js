const { user } = require('../models/user');
const UserService = require('../services/userService');


const createUserValid = (req, res, next) => {
    const data = req.body;
    const errMessage = validateFields(data) ||
      checkName(data)||
      validatePassword(data)||
      checkUserExists(data)||
      checkByRegex(data.email, /(\W|^)[\w.+\-]*@gmail\.com(\W|$)/,
        `E-mail must be like *@gmail.com`)||
      checkByRegex(data.phoneNumber, /^(\+38)(0\d{9})$/,
        `Phone must be like +380xxxxxxxxx`);

    if (errMessage) {
        const error = {
            error: true,
            message: errMessage
        };
        res.status(400).send(error);
    } else {
        next();
    }
}

const updateUserValid = (req, res, next) => {
    const data = req.body;
    const errMessage = validateFields(data) ||
      checkName(data)||
      validatePassword(data)||
      checkByRegex(data.email, /(\W|^)[\w.+\-]*@gmail\.com(\W|$)/,
        `E-mail must be like *@gmail.com`)||
      checkByRegex(data.phoneNumber, /^(\+38)(0\d{9})$/,
        `Phone must be like +380xxxxxxxxx`);

    if (errMessage) {
        const error = {
            error: true,
            message: errMessage
        };
        res.status(400).send(error);
    } else {
        next();
    }
}

function checkUserExists(data) {
    const fighter = UserService.getOne(
      {firstName:data.firstName, lastName: data.lastName, email:data.email,
                phoneNumber:data.phoneNumber, password: data.password})
    if (fighter){
        return 'This user entity exists';
    }
}

function checkName(data){
    let res = '';
    res = res || checkOnlyLetters(data.firstName) || checkOnlyLetters(data.lastName);
    return res;
}

function checkOnlyLetters(data){
    const isContainLetters = /^[a-zA-Z]+$/.test(data.toString());
    if (!isContainLetters||data.length < 3 ) {
        return `The \'${data}\' must contain only letters and be more than 2 symbols`;
    }
}

function validatePassword(data){
    if (data.password.length < 3 ) {
        return `The password must contain more than 2 symbols`;
    }
}

function compareKeys(a, b) {
    const aKeys = Object.keys(a).sort();
    const bKeys = Object.keys(b).sort();
    return JSON.stringify(aKeys) === JSON.stringify(bKeys);
}

function validateFields(data) {
    const clone = Object.assign({}, user);
    delete clone.id;
    if (!compareKeys(data, clone)) {
        return `The fields must be firstName, lastName, email, phoneNumber, password`;
    }
}

function checkByRegex(data, regex, message){
    const re = regex;
    if(!re.test(String(data).toLowerCase())){
        return message;
    }
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;