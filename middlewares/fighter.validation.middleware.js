const { fighter } = require('../models/fighter');
const FighterService = require('../services/fighterService');

const createFighterValid = (req, res, next) => {
    const data = req.body;
    const errMessage = validateFields(data) ||
      checkNumberFields(data)||
      validateName(data) ||
      checkFighterExists(data);
    if (errMessage) {
        const error = {
            error: true,
            message: errMessage
        };
        res.status(400).send(error);
    } else {
        next();
    }
}

const updateFighterValid = (req, res, next) => {
    const data = req.body;
    const errMessage = validateFields(data) ||
      checkNumberFields(data)||
      validateName(data);
    if (errMessage) {
        const error = {
            error: true,
            message: errMessage
        };
        res.status(400).send(error);
    } else {
        next();
    }
}

function checkFighterExists(data) {
    const fighter = FighterService.getOne({name:data.name, health: data.health, defense:data.defense, power:data.power})
    if (fighter){
        return 'This fighter entity exists';
    }
}

function validateName(data){
    const iscontainLetters = /^[a-zA-Z]+$/.test(data.name.toString());
    if (!iscontainLetters||data.name.length < 3 ) {
        return `The name must contain only letters and be more than 2 symbols`;
    }
}

function checkNumberFields(data){
    const keys = [
        {field:"health", min:"1",max:"100"},
        {field:"defense", min:"1",max:"10"},
        {field:"power", min:"1",max:"100"}]
    let res= "";
    for(let i in keys){
        res = res || checkRange(+data[keys[i].field], keys[i].max, keys[i].min, keys[i].field )
    }
    return res;
}

function checkRange(data, max, min, field){
    const checkRange = data > max || data < min;
    if (!Number.isInteger(data)|| checkRange ) {
        return `${field} must be integer, the range is [${min}, ${max}]`;
    }
}

function compareKeys(a, b) {
    const aKeys = Object.keys(a).sort();
    const bKeys = Object.keys(b).sort();
    return JSON.stringify(aKeys) === JSON.stringify(bKeys);
}

function validateFields(data) {
    const clone = Object.assign({}, fighter);
    delete clone.id;
    if (!compareKeys(data, clone)) {
        return `The fields must be name, health, defense, power`;
    }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;