const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', (req, res, next) => {
  try {
    const data = UserService.getAll();
    if(!data){
      throw Error('Users are not found');
    }
    res.data = data;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
  try {
    const data = UserService.getOne({id: req.params.id});
    if(!data){
      throw Error('User is not found');
    }
    res.data = data;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.post('/', createUserValid, (req, res, next) => {
  try {
    const data = UserService.create(req.body);
    if(!data){
      throw Error('User is not created');
    }
    res.data = data;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.put('/:id', updateUserValid, (req, res, next) => {
  try {
    const data = UserService.update(req.params.id, req.body);
    if(!data){
      throw Error('User is not updated. Check Id.');
    }
    res.data = data;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
  try {
    const data = UserService.delete(req.params.id);
    if(!data){
      throw Error('Error while deleting. Check Id');
    }
    res.data = data;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

module.exports = router;