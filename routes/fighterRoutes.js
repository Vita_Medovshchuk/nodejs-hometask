const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get('/', (req, res, next) => {
  try {
    const data = FighterService.getAll();
    if(!data){
      throw Error('Fighters are not found');
    }
    res.data = data;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
  try {
    const data = FighterService.getOne({id: req.params.id});
    if(!data){
      throw Error('Fighter is not found');
    }
    res.data = data;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.post('/', createFighterValid, (req, res, next) => {
  try {
    const data = FighterService.create(req.body);
    if(!data){
      throw Error('Fighter is not created');
    }
    res.data = data;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.put('/:id', updateFighterValid, (req, res, next) => {
  try {
    const data = FighterService.update(req.params.id, req.body);
    if(!data){
      throw Error('Fighter is not updated. Check Id.');
    }
    res.data = data;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
  try {
    const data = FighterService.delete(req.params.id);
    if(!data){
      throw Error('Error while deleting. Check Id');
    }
    res.data = data;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

module.exports = router;