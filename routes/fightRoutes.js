const { Router } = require('express');
const FightService = require('../services/fightService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');


const router = Router();


router.get('/', (req, res, next) => {
  try {
    const data = FightService.getAll();
    if(!data){
      throw Error('Fights are not found');
    }
    res.data = data;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
  try {
    const data = FightService.getOne({id: req.params.id});
    if(!data){
      throw Error('Fight is not found');
    }
    res.data = data;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.post('/', (req, res, next) => {
  try {
    const data = FightService.create(req.body);
    if(!data){
      throw Error('Fight is not created');
    }
    res.data = data;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.put('/:id', (req, res, next) => {
  try {
    const data = FightService.update(req.params.id, req.body);
    if(!data){
      throw Error('Fight is not updated. Check Id.');
    }
    res.data = data;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
  try {
    const data = FightService.delete(req.params.id);
    if(!data){
      throw Error('Error while deleting. Check Id');
    }
    res.data = data;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);
module.exports = router;