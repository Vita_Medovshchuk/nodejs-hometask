const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
  getAll() {
    const fighters = FighterRepository.getAll();
    if(!fighters) {
      return null;
    }
    return fighters;
  }

  getOne(search) {
    const fighter = FighterRepository.getOne(search);
    if(!fighter) {
      return null;
    }
    return fighter;
  }

  create(data) {
    const fighter = FighterRepository.create(data);
    if(!fighter) {
      return null;
    }
    return fighter;
  }

  update(id, dataToUpdate) {
    if(!this.getOne({id})) {
      return null;
    }
    const updated = FighterRepository.update(id, dataToUpdate);
    return updated;
  }

  delete(id) {
    if(!this.getOne({id})) {
      return null;
    }
    const deleted = FighterRepository.delete(id);
    return deleted;
  }
}

module.exports = new FighterService();