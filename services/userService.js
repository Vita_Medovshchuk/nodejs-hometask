const { UserRepository } = require('../repositories/userRepository');

class UserService {

    getAll() {
        const fighters = UserRepository.getAll();
        if(!fighters) {
            return null;
        }
        return fighters;
    }

    getOne(search) {
        const fighter = UserRepository.getOne(search);
        if(!fighter) {
            return null;
        }
        return fighter;
    }

    create(data) {
        const fighter = UserRepository.create(data);
        if(!fighter) {
            return null;
        }
        return fighter;
    }

    update(id, dataToUpdate) {
        if(!this.getOne({id})) {
            return null;
        }
        const updated = UserRepository.update(id, dataToUpdate);
        return updated;
    }

    delete(id) {
        if(!this.getOne({id})) {
            return null;
        }
        const deleted = UserRepository.delete(id);
        return deleted;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();