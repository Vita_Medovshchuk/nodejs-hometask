const { FightRepository } = require('../repositories/fightRepository');

class FightersService {
  getAll() {
    const fights = FightRepository.getAll();
    if(!fights) {
      return null;
    }
    return fights;
  }

  getOne(search) {
    const fight = FightRepository.getOne(search);
    if(!fight) {
      return null;
    }
    return fight;
  }

  create(data) {
    const fight = FightRepository.create(data);
    if(!fight) {
      return null;
    }
    return fight;
  }

  update(id, dataToUpdate) {
    if(!this.getOne({id})) {
      return null;
    }
    const updated = FightRepository.update(id, dataToUpdate);
    return updated;
  }

  delete(id) {
    if(!this.getOne({id})) {
      return null;
    }
    const deleted = FightRepository.delete(id);
    return deleted;
  }
}

module.exports = new FightersService();